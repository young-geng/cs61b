
public class Node {
  public Node next;
  public int r, g, b;
  public int repeat;

  public Node(int r, int g, int b, int repeat, Node next) {
    this.next = next;
    this.repeat = repeat;
    this.r = r;
    this.g = g;
    this.b = b;
  }
  public boolean repeats() {
    return r == next.r && g == next.g && b == next.b;
  }

  public void merge() throws IllegalArgumentException{
    if (!repeats()) {
      throw new IllegalArgumentException();
    }
    repeat += next.repeat;
    next = next.next;
  }
}