public class BadTransactionException extends Exception {

  public String message;  // The invalid account number.

  /**
   *  Creates an exception object for nonexistent account "badAcctNumber".
   **/
  public BadTransactionException(String message) {
    super("Invalid transaction: " + message);
    this.message = message;
  }
}