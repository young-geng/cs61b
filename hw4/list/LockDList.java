package list;
public class LockDList extends DList {


    protected DListNode newNode(Object item, DListNode prev, DListNode next) {
        return new LockDListNode(item, prev, next);
    }

    public void lockNode(DListNode node) {
        ((LockDListNode)node).lock();
    }


    public void remove(DListNode node) {
        if (!((LockDListNode)node).locked) {
            super.remove(node);
        }
    
    }
}