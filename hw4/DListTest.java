import list.DList;
import list.DListNode;
import list.LockDListNode;
import list.LockDList;

public class DListTest {
    public static void main(String[] args) {
        LockDList d = new LockDList();
        d.insertFront(new Integer(1));
        d.insertFront(new Integer(2));
        d.insertBack(new Integer(0));
        System.out.println(d + "");
        System.out.println(d.length());
        DListNode dn = d.front();
        System.out.println(dn.item);
        dn = d.next(dn);
        System.out.println(dn.item);
        d.lockNode(dn);
        d.remove(dn);
        System.out.println(d + "");
    }
}