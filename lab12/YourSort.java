/* YourSort.java */

public class YourSort {


  public static void sort(int[] A) {
    // Place your Part III code here.
    qsort(A, 0, A.length - 1, 0, (int)(Math.log(A.length)));

  }

  public static void qsort(int[] a, int start, int end, int depth, int logLength) {
  	if (start >= end) {
  		return;
  	} else if (depth < logLength) {
  		heapsort(a, start, end);
  		return;
  	}
  	int low = start, high = end, pivot = a[start];
  	while (low < high) {
  		while (low < high && a[high] >= pivot)
  			high--;
  		if (low < high) 
  			a[low] = a[high];
  		while (low < high && a[low] <= pivot)
  			low++;
  		if (low < high)
  			a[high] = a[low];
  	}
  	a[low] = pivot;
  	qsort(a, start, low - 1, depth + 1, logLength);
  	qsort(a, high + 1, end, depth + 1, logLength);
  }




  public static void insertionSort(int[] a, int start, int end) {
	  for (int p = start; p <= end; p++) {
	      int tmp = a[p];
	      int j = p;

	      for(; (j > 0) && (tmp < a[j - 1]); j--) {
	        a[j] = a[j - 1];
	      }
	      a[j] = tmp;
	    }
  }

  public static void heapsort(int[] a, int start, int end) {
    for (int i = (end - start) / 2; i >= start; i--) {
      percDown(a, i, end);
    }
    for (int i = end; i > start; i--) {
      int temp = a[start];
      a[start] = a[i];
      a[i] = a[start];
      percDown(a, start, end);
    }
  }

  /**
   *  Internal method for heapsort.
   *  @param i the index of an item in the heap.
   *  @return the index of the left child.
   **/
  private static int leftChild(int i) {
    return 2 * i + 1;
  }

  /**
   *  Internal method for heapsort, used in deleteMax and buildHeap.
   *  @param a an array of int items.
   *  @index i the position from which to percolate down.
   *  @int n the logical size of the binary heap.
   **/
  private static void percDown(int[] a, int i, int n) {
    int child;
    int tmp;

    for (tmp = a[i]; leftChild(i) < n; i = child) {
      child = leftChild(i);
      if ((child != n - 1) && (a[child] < a[child + 1])) {
        child++;
      }
      if (tmp < a[child]) {
        a[i] = a[child];
      } else {
        break;
      }
    }
    a[i] = tmp;
  }






}
