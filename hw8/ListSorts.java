/* ListSorts.java */

import list.*;

public class ListSorts {

  private final static int SORTSIZE = 100;

  /**
   *  makeQueueOfQueues() makes a queue of queues, each containing one item
   *  of q.  Upon completion of this method, q is empty.
   *  @param q is a LinkedQueue of objects.
   *  @return a LinkedQueue containing LinkedQueue objects, each of which
   *    contains one object from q.
   **/
  public static LinkedQueue makeQueueOfQueues(LinkedQueue q) throws QueueEmptyException {
    // Replace the following line with your solution.
    LinkedQueue queueOfQueues = new LinkedQueue();
    while (!q.isEmpty()) {
      LinkedQueue tq = new LinkedQueue();
      tq.enqueue(q.dequeue());
      queueOfQueues.enqueue(tq);
    }
    return queueOfQueues;
  }

  /**
   *  mergeSortedQueues() merges two sorted queues into a third.  On completion
   *  of this method, q1 and q2 are empty, and their items have been merged
   *  into the returned queue.
   *  @param q1 is LinkedQueue of Comparable objects, sorted from smallest 
   *    to largest.
   *  @param q2 is LinkedQueue of Comparable objects, sorted from smallest 
   *    to largest.
   *  @return a LinkedQueue containing all the Comparable objects from q1 
   *   and q2 (and nothing else), sorted from smallest to largest.
   **/
  @SuppressWarnings("unchecked")
  public static LinkedQueue mergeSortedQueues(LinkedQueue q1, LinkedQueue q2) throws QueueEmptyException {
    // Replace the following line with your solution.
    LinkedQueue q = new LinkedQueue();
    while (!q1.isEmpty() && !q2.isEmpty()) {
      int comp = ((Comparable)(q1.front())).compareTo((Comparable)(q2.front()));
      if (comp <= 0) {
        q.enqueue(q1.dequeue());
      } else {
        q.enqueue(q2.dequeue());
      }
    }
    while (!q1.isEmpty()) {
      q.enqueue(q1.dequeue());
    }
    while (!q2.isEmpty()) {
      q.enqueue(q2.dequeue());
    }
    return q;
  }

  /**
   *  partition() partitions qIn using the pivot item.  On completion of
   *  this method, qIn is empty, and its items have been moved to qSmall,
   *  qEquals, and qLarge, according to their relationship to the pivot.
   *  @param qIn is a LinkedQueue of Comparable objects.
   *  @param pivot is a Comparable item used for partitioning.
   *  @param qSmall is a LinkedQueue, in which all items less than pivot
   *    will be enqueued.
   *  @param qEquals is a LinkedQueue, in which all items equal to the pivot
   *    will be enqueued.
   *  @param qLarge is a LinkedQueue, in which all items greater than pivot
   *    will be enqueued.  
   **/
  @SuppressWarnings("unchecked")   
  public static void partition(LinkedQueue qIn, Comparable pivot, 
                               LinkedQueue qSmall, LinkedQueue qEquals, 
                               LinkedQueue qLarge) {
    // Your solution here.
    try {
      while (!qIn.isEmpty()) {
        int cmp = ((Comparable)(qIn.front())).compareTo(pivot);
        if (cmp < 0) {
          qSmall.enqueue(qIn.dequeue());
        } else if (cmp == 0) {
          qEquals.enqueue(qIn.dequeue());
        } else {
          qLarge.enqueue(qIn.dequeue());
        }
      }
    } catch (QueueEmptyException e) {
      System.err.println("Empty Queue Accessed!");
      System.exit(1);
    }
  }

  /**
   *  mergeSort() sorts q from smallest to largest using mergesort.
   *  @param q is a LinkedQueue of Comparable objects.
   **/
  @SuppressWarnings("unchecked")
  public static void mergeSort(LinkedQueue q) {
    // Your solution here.
    try {
      LinkedQueue q0 = makeQueueOfQueues(q);
      while (q0.size() > 1) {
        LinkedQueue q1 = (LinkedQueue)(q0.dequeue());
        LinkedQueue q2 = (LinkedQueue)(q0.dequeue());
        q0.enqueue(mergeSortedQueues(q1, q2));
      }
      if (q0.size() == 1) {
        LinkedQueue q1 = (LinkedQueue)(q0.dequeue());
        q.append(q1);
      }
    } catch (QueueEmptyException e) {
      System.exit(1);
    }
  }

  /**
   *  quickSort() sorts q from smallest to largest using quicksort.
   *  @param q is a LinkedQueue of Comparable objects.
   **/
  public static void quickSort(LinkedQueue q) {
    // Your solution here.
    if (q.size() <= 1) {
      return;
    }
    int rand = (int) (q.size() * Math.random());
    if (rand == 0) {
      rand++;
    }
    Comparable pivot = (Comparable)(q.nth(rand));
    LinkedQueue qSmall = new LinkedQueue();
    LinkedQueue qEquals = new LinkedQueue();
    LinkedQueue qLarge = new LinkedQueue();
    partition(q, pivot, qSmall, qEquals, qLarge);
    quickSort(qSmall);
    quickSort(qLarge);
    q.append(qSmall);
    q.append(qEquals);
    q.append(qLarge);

  }

  /**
   *  makeRandom() builds a LinkedQueue of the indicated size containing
   *  Integer items.  The items are randomly chosen between 0 and size - 1.
   *  @param size is the size of the resulting LinkedQueue.
   **/
  public static LinkedQueue makeRandom(int size) {
    LinkedQueue q = new LinkedQueue();
    for (int i = 0; i < size; i++) {
      q.enqueue(new Integer((int) (size * Math.random())));
    }
    return q;
  }

  /**
   *  main() performs some tests on mergesort and quicksort.  Feel free to add
   *  more tests of your own to make sure your algorithms works on boundary
   *  cases.  Your test code will not be graded.
   **/
  public static void main(String [] args) {


    LinkedQueue q = makeRandom(1);
    //LinkedQueue q = new LinkedQueue();
    System.out.println(q.toString());
    mergeSort(q);
    System.out.println(q.toString());

    q = makeRandom(1);
    q = new LinkedQueue();
    System.out.println(q.toString());
    quickSort(q);
    System.out.println(q.toString());


    Timer stopWatch = new Timer();
    q = makeRandom(SORTSIZE);
    stopWatch.start();
    mergeSort(q);
    stopWatch.stop();
    System.out.println("Mergesort time, " + SORTSIZE + " Integers:  " +
                       stopWatch.elapsed() + " msec.");

    stopWatch.reset();
    q = makeRandom(SORTSIZE);
    stopWatch.start();
    quickSort(q);
    stopWatch.stop();
    System.out.println("Quicksort time, " + SORTSIZE + " Integers:  " +
                       stopWatch.elapsed() + " msec.");
    
  }

}
