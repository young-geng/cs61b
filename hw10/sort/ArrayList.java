/* This program is an ArrayList class built for CS61B.
   As the name implies, this class should be compatible with java.util.ArrayList.
   Besides, the author, Young, is extremely angry about the policy of banning java.util.* in CS61B class.
 */

package sort;

import java.util.NoSuchElementException;
import java.util.Iterator;

public class ArrayList<E> 
    //extends java.util.AbstractList<E> 
    implements Iterable<E> //, Collection<E>, List<E>, RandomAccess 
{
    private int size;
    private Object[] list;
    private int maxLength;
    

    public ArrayList() {
        this(100);
    }

    public ArrayList(int defaultLength) {
        maxLength = defaultLength;
        size = 0;
        list = new Object[maxLength];
    }

    public ArrayList(E[] array) {
        size = array.length;
        list = new Object[array.length];
        for (int i = 0; i < size; i++) {
            list[i] = (Object)array[i];
        }
        maxLength = array.length;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    
    public Object[] toArray() {
        Object[] array = new Object[size];
        for (int i = 0; i < size; i++) {
            array[i] = list[i];
        }
        return array;
    }

    public int size() {
        return size;
    }

    private void reallocateLength() {
        int newLength = maxLength * 2;
        Object[] newList = new Object[newLength];
        for (int i = 0; i < size; i++) {
            newList[i] = list[i];
        }
        list = newList;
        maxLength = newLength;
    }

    private void reallocateLength(int length) {
        int newLength = length;
        Object[] newList = new Object[newLength];
        for (int i = 0; i < size; i++) {
            newList[i] = list[i];
        }
        list = newList;
        maxLength = newLength;
    }

    public void add(E obj) {
        size++;
        if (size == maxLength) {
            reallocateLength();
        }
        list[size - 1] = (Object)obj;
    }

    public void add(int index, E obj) {
        if (index < size) {
            size++;
        } else {
            size = index + 1;
        }
        if (index > maxLength * 2) {
            reallocateLength(index + 1);
        } else if (size == maxLength) {
            reallocateLength();
        }
        for (int i = size - 2; i >= index; i--) {
            list[i + 1] = list[i];
        }
        list[index] = (Object)obj;
    }

    @SuppressWarnings("unchecked")
    public E get(int index) throws IndexOutOfBoundsException{
        if (index > size) {
            throw new IndexOutOfBoundsException();
        }
        return (E)list[index];
    }

    @SuppressWarnings("unchecked")
    public E remove (int index) throws IndexOutOfBoundsException {
        if (index > size) {
            throw new IndexOutOfBoundsException();
        }
        E element = (E)list[index];
        size--;
        for (int i = index; i < size; i++) {
            list[i] = list[i + 1];
        }
        return element;
    }

    public boolean contains(E element) {
        return find(element) != -1;
    }

    @SuppressWarnings("unchecked")
    public E set(int index, E element) {
        E tmp;
        if (index >= size) {
            size = index + 1;
        }
        if (index < maxLength) {
            tmp = (E)list[index];
            list[index] = (Object)element;
        } else {
            reallocateLength(index + 1);
            list[index] = (Object)element;
            tmp = null;
        }
        return tmp;
    }

    @SuppressWarnings("unchecked")
    public Iterator<E> iterator(){
        return (Iterator<E>)new ArrayListIterator<E>(list, size);
    }

    public void clear() {
        size = 0;
        maxLength = 100;
        reallocateLength(maxLength);
    }

    @SuppressWarnings("unchecked")
    public int find(E element) {
        for (int i = 0; i < size; i++) {
            if (list[i] != null && ((E)list[i]).equals(element)) {
                return i;
            }
        }
        return -1;
    }

    public String toString() {
        String s = "[";
        for (E i : this) {
            s += i + ", ";
        }
        return s + "]";
    }
}


class ArrayListIterator<E>
    implements Iterator<E>
{
    private Object[] list;
    private int size;
    private int index;

    public ArrayListIterator(Object[] list, int size) {
        this.list = list;
        this.size = size;
        index = 0;
    }

    public boolean hasNext() {
        return index < size;
    }

    @SuppressWarnings("unchecked")
    public E next() throws NoSuchElementException{
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        index++;
        return (E)list[index - 1];
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }

}
