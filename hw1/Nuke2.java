import java.util.Scanner;

public class Nuke2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        if (in.hasNextLine()) {
            String line = in.nextLine();
            if (line.length() < 2)
                System.exit(1);
            System.out.println(line.charAt(0) + line.substring(2));
        } else {
            System.exit(1);
        }
    }
}
